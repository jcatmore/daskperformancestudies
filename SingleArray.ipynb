{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "6d0eeb06",
   "metadata": {},
   "source": [
    "# Scaling out with Dask: introduction\n",
    "\n",
    "The file we used in the previous examples is one of several thousand sitting on disk. To process them all one after another on a single core would be prohibitively slow. Instead we can use up to 256 cores and process the events in parallel. In this session we'll demonstrate how to scale out to multiple compute cores using the Dask framework with AwkwardArray and RDataFrame. This will be a gentle introduction before we embark on a more complete analysis, which is sufficiently complex that it is best implemented as flat code rather than in Jupyter notebooks.\n",
    "\n",
    "When you execute a Dask command, instead of immediately running the command, it instead builds up a computational graph and the entire graph is only executed when the `compute` method is used on the final object. At this point the data is read from disk (either all at once or in chunks as required) and can also be scaled out to many cores. \n",
    "\n",
    "The best way to work is to do exploration of data, and the first set-up of the computational task without dask, and on a single file, probably line-by-line as demonstrated in the previous work book. As you'll see below, the transition to Dask is then trivial, and having tested this with a single file, you can then scale up to the full set of files. In the workbook below you'll run the same invariant mass task as was set up in the previous exercise, but over 1000 files. You can run the job twice, once for a single file, and once for many files. \n",
    "\n",
    "\n",
    "## Using AwkwardArray\n",
    "\n",
    "Again we start with the usual includes, but there is an extra one - `dask_awkward`, which behaves just like awkward array but executes lazily, e.g. only once the `compute` method is used."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1d20bfc5",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import Awkward Array\n",
    "import awkward as ak\n",
    "# Import uproot\n",
    "import uproot\n",
    "# Import NumPy\n",
    "import numpy as np\n",
    "# Import MatPlotLib\n",
    "import matplotlib.pyplot as plt\n",
    "# Import dask-awkward and dask\n",
    "import dask_awkward as dak\n",
    "import dask\n",
    "# For four-momentum calculations\n",
    "import vector\n",
    "# For timing measurements\n",
    "import time"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "079d9680",
   "metadata": {},
   "outputs": [],
   "source": [
    "#dataset = \"/storage/shared/data/PHYSLITEforML/./mc21_13p6TeV.513103.MGPy8EG_Wtaunu_H_FxFx3jHT2bias_SW_CFilterBVeto.deriv.DAOD_PHYSLITE.e8453_s3873_r13829_p5631/DAOD_PHYSLITE.*.pool.root.1\"\n",
    "#dataset = \"/storage/shared/data/PHYSLITEforML/./mc21_13p6TeV.601353.PhPy8EG_tW_dyn_DR_dil_antitop.deriv.DAOD_PHYSLITE.e8453_s3873_r13829_p5631/DAOD_PHYSLITE.*.pool.root.1\" \n",
    "#dataset = \"/storage/shared/data/PHYSLITEforML/./mc21_13p6TeV.700693.Sh_2212_jj_JZ6.deriv.DAOD_PHYSLITE.e8472_s3873_r13829_p5631/DAOD_PHYSLITE.*.pool.root.1\"\n",
    "#dataset = \"/storage/shared/data/PHYSLITEforML/./mc21_13p6TeV.801170.Py8EG_A14NNPDF23LO_jj_JZ5.deriv.DAOD_PHYSLITE.e8453_s3873_r13829_p5631/DAOD_PHYSLITE.*.pool.root.1\"\n",
    "#dataset = \"/storage/shared/data/PHYSLITEforML/./mc21_13p6TeV.601190.PhPy8EG_AZNLO_Zmumu.deriv.DAOD_PHYSLITE.e8453_s3873_r13829_p5631/DAOD_PHYSLITE.*.pool.root.1\"\n",
    "dataset = \"/storage/shared/data/PHYSLITEforML/./mc21_13p6TeV.801278.Py8EG_A14NNPDF23LO_perf_JF17.deriv.DAOD_PHYSLITE.e8453_s3873_r13829_p5631/DAOD_PHYSLITE.*.pool.root.1\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "112e9064",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Number of Dask workers\n",
    "n_workers = 8"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bf6c9e11",
   "metadata": {},
   "source": [
    "### Many files\n",
    "\n",
    "We now repeat the above exercise, except with all of the files in the directory. The switch is very simple - just the directory path has to change, and also a dask cluster has to be set up in the next cell"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "027aae7f",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set up a dask cluster \n",
    "# We set the number of workers to 64 and have a single execution thread per worker\n",
    "# Thus each core independently works on its own batch of data\n",
    "# The memory per process has to be carefully chosen - too small and the job will be slowed down due to over \n",
    "# frequent reads from disk. Too large and the machine will run out of memory and start to swap data back and \n",
    "# forth from disk \n",
    "from dask.distributed import Client,LocalCluster\n",
    "cluster = LocalCluster(n_workers=n_workers,processes=True,threads_per_worker=1, memory_limit=\"10GiB\")\n",
    "client = Client(cluster)\n",
    "client"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "74254418",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Open the files\n",
    "# Note that for multiple files wildcards are allowed\n",
    "manyFiles = dataset+\":CollectionTree\"\n",
    "variables = [\"AnalysisMuonsAuxDyn.pt\",\n",
    "             \"AnalysisMuonsAuxDyn.eta\",\n",
    "             \"AnalysisMuonsAuxDyn.phi\",\n",
    "             \"AnalysisMuonsAuxDyn.charge\"\n",
    "            ]\n",
    "events = uproot.dask(manyFiles, \n",
    "                     library=\"ak\", \n",
    "                     filter_name=variables,\n",
    "                     open_files=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "58684974",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Now everything else is the same with the previous single-file attempt, except the vector-based calculation\n",
    "# of the invariant mass is replaced with a simpler massless approximation as the vector package doesn't seem to\n",
    "# work with dask...\n",
    "muons = dak.zip(\n",
    "    {\n",
    "        \"pt\": events[\"AnalysisMuonsAuxDyn.pt\"],\n",
    "        \"eta\": events[\"AnalysisMuonsAuxDyn.eta\"],\n",
    "        \"phi\": events[\"AnalysisMuonsAuxDyn.phi\"],\n",
    "        \"charge\": events[\"AnalysisMuonsAuxDyn.charge\"],\n",
    "    }\n",
    ")\n",
    "mask_two_muons = dak.count(muons.pt,axis=1) > 1\n",
    "muons_selected_events = muons[mask_two_muons]\n",
    "muon_pairs = dak.combinations(muons_selected_events,2,axis=1)\n",
    "mu1, mu2 = dak.unzip(muon_pairs)\n",
    "masses = np.sqrt(\n",
    "    2 * mu1.pt * mu2.pt * (np.cosh(mu1.eta - mu2.eta) - np.cos(mu1.phi - mu2.phi))\n",
    ")\n",
    "mask_opposite_charge = mu1.charge != mu2.charge\n",
    "masses_final = masses[mask_opposite_charge]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0e32a2ea",
   "metadata": {},
   "outputs": [],
   "source": [
    "# These lines are an exact copy of the ones in the previous workbook to calculate the invariant mass,\n",
    "# except ak is used instead of dak and no execution happens in this cell\n",
    "# Not working due to the vector calculation\n",
    "#muons = dak.zip(\n",
    "#    {\n",
    "#        \"pt\": events[\"AnalysisMuonsAuxDyn.pt\"],\n",
    "#        \"eta\": events[\"AnalysisMuonsAuxDyn.eta\"],\n",
    "#        \"phi\": events[\"AnalysisMuonsAuxDyn.phi\"],\n",
    "#        \"charge\": events[\"AnalysisMuonsAuxDyn.charge\"],\n",
    "#    }\n",
    "#)\n",
    "#mask_two_muons = dak.count(muons.pt,axis=1) > 1\n",
    "#muons_selected_events = muons[mask_two_muons]\n",
    "#muon_pairs = dak.combinations(muons_selected_events,2,axis=1)\n",
    "#mu1, mu2 = dak.unzip(muon_pairs)\n",
    "\n",
    "#vector.register_awkward()\n",
    "#p1 = ak.zip(\n",
    "#    {\n",
    "#        \"pt\": mu1.pt,\n",
    "#        \"eta\": mu1.eta,\n",
    "#        \"phi\": mu1.phi,\n",
    "#        \"M\": ak.full_like(mu1.charge, 105.66),\n",
    "#    }, \n",
    "#    with_name=\"Momentum4D\",\n",
    "#)\n",
    "#p2 = ak.zip(\n",
    "#    {\n",
    "#        \"pt\": mu2.pt,\n",
    "#        \"eta\": mu2.eta,\n",
    "#        \"phi\": mu2.phi,\n",
    "#        \"M\": ak.full_like(mu2.charge, 105.66),\n",
    "#    }, \n",
    "#    with_name=\"Momentum4D\",\n",
    "#)\n",
    "#masses = (p1+p2).M\n",
    "#mask_opposite_charge = mu1.charge != mu2.charge\n",
    "#masses_final = masses[mask_opposite_charge]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "75385e3c",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Do the work - this will take some minutes\n",
    "start = time.time()\n",
    "#evaluated = dask.compute([masses_final])\n",
    "#masses_for_plotting = ak.ravel(evaluated[0])\n",
    "masses_for_plotting = ak.ravel(masses_final.compute())\n",
    "end = time.time()\n",
    "print(\"Time taken to execute = \",end-start)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a1486bbb",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plotting (one can even build histograms in dask but this isn't necessary here)\n",
    "plt.hist(masses_for_plotting, bins=100, range=(0.0, 150000.0))\n",
    "plt.show() "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6a9b3be3",
   "metadata": {},
   "source": [
    "## RDataFrame and Dask"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "38479f6a",
   "metadata": {},
   "source": [
    "RDataFrame works with Dask in much the same way as AwkwardArray - in fact, since it is based on lazily executed computational graphs, the transition is even more straightforward. This is how we do it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5cc91669",
   "metadata": {},
   "outputs": [],
   "source": [
    "# ROOT import\n",
    "import ROOT"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1ae6ead3",
   "metadata": {},
   "outputs": [],
   "source": [
    "# We need to make sure that the C++ libraries are loaded to each process. \n",
    "# This is achieved by defining a new initialise method that gets called by each process.\n",
    "initialize = ROOT.RDF.Experimental.Distributed.initialize\n",
    "\n",
    "def myinit():\n",
    "    ROOT.gSystem.AddDynamicPath(\"./commontools/.\")\n",
    "    ROOT.gROOT.ProcessLine(\".include ./commontools\");\n",
    "    ROOT.gROOT.ProcessLine(\".L ./commontools/example.cxx+\")\n",
    "    ROOT.gSystem.Load(\"./commontools/example_cxx.so\") # Library with the myFilter function\n",
    "\n",
    "initialize(myinit)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1b469e46",
   "metadata": {},
   "outputs": [],
   "source": [
    "#connection = create_connection(n_workers)\n",
    "RDataFrame = ROOT.RDF.Experimental.Distributed.Dask.RDataFrame\n",
    "df = RDataFrame(\"CollectionTree\", \n",
    "                dataset,\n",
    "                daskclient=client)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c9061f4b",
   "metadata": {},
   "outputs": [],
   "source": [
    "df_two_muons = df.Filter(\"AnalysisMuonsAuxDyn.pt.size() > 1\", \"At least two muons\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "deb0451b",
   "metadata": {},
   "outputs": [],
   "source": [
    "df_two_muons = df_two_muons.Define(\"m_mumu\", \n",
    "                                   \"ExampleInvariantMass(AnalysisMuonsAuxDyn.pt,AnalysisMuonsAuxDyn.eta,AnalysisMuonsAuxDyn.phi,AnalysisMuonsAuxDyn.charge,105.66)\"\n",
    "                                  )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9b8e0f5a",
   "metadata": {},
   "outputs": [],
   "source": [
    "c = ROOT.TCanvas()\n",
    "hist = df_two_muons.Histo1D((\"m_mumu\",\"m_mumu\",100,0,150000.0), \"m_mumu\")  \n",
    "start = time.time()\n",
    "hist.Draw()\n",
    "end = time.time()\n",
    "print(\"Time taken to execute = \",end-start)\n",
    "c.Draw()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7f2460f7",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
