# DaskPerformanceStudies

Scripts for comparing the performance of AwkwardArray and RDataFrame when run with DASK.

## Contents

* `singleArray.ipynb` : compares time (AA vs RDF) to evaluate a single array
* `AnalysisAA.ipynb` : evaluates time to perform an analysis with many arrays and a complicated computational graph with AA
* `AnalysisRDF.ipynb` : evaluates time to perform an analysis with many arrays and a complicated computational graph with RDF
* `commontools` : C++ methods used by RDF 
* `*.py` : stand-alone python versions of the above Jupyter notebooks 
